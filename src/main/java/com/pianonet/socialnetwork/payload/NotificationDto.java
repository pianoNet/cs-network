package com.pianonet.socialnetwork.payload;


import com.pianonet.socialnetwork.model.NotificationType;
import com.pianonet.socialnetwork.model.User;
import lombok.*;

import java.util.Date;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@EqualsAndHashCode
public class NotificationDto {
    private long id;
    private NotificationType notificationType;
    private Long eventId;
    private boolean status;
    private User author;
    private User recipient;
    private Date creatDate;

}
