package com.pianonet.socialnetwork.mappers;

import com.pianonet.socialnetwork.model.Comment;
import com.pianonet.socialnetwork.payload.CommentRequest;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;

@Component
public class CommentMapper extends ConfigurableMapper {
    @Override
    protected void configure(MapperFactory factory) {

        factory.classMap(Comment.class, CommentRequest.class)
                .byDefault()
                .register();
    }
}
