package com.pianonet.socialnetwork.service.impl;


import com.pianonet.socialnetwork.exception.ResourceNotFoundException;
import com.pianonet.socialnetwork.mappers.NotificationMapper;
import com.pianonet.socialnetwork.mappers.UserMapper;
import com.pianonet.socialnetwork.model.Notification;
import com.pianonet.socialnetwork.model.NotificationType;
import com.pianonet.socialnetwork.model.User;
import com.pianonet.socialnetwork.payload.NotificationDto;
import com.pianonet.socialnetwork.payload.UserDto;
import com.pianonet.socialnetwork.repository.NotificationRepository;
import com.pianonet.socialnetwork.service.NotificationService;
import com.pianonet.socialnetwork.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class NotificationServiceImpl implements NotificationService {
    private final NotificationRepository notificationRepository;
    private final NotificationMapper notificationMapper;
    private final UserService userService;
    private final UserMapper userMapper;

    @Override
    public void addNotification(Notification notification) {
        notificationRepository.save(notification);
    }

    @Override
    public List<NotificationDto> getPostNotificationByFriends(List<UserDto> friendsDto) {
        List<User> friends = userMapper.mapAsList(friendsDto, User.class);
        List<Notification> notifications = notificationRepository.getPostNotificationByFriends(friends, NotificationType.POST);
        return notificationMapper.mapAsList(notifications, NotificationDto.class);
    }

    @Override
    public void editStatus(Long id) {
        Notification notification = notificationRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException(" Notification does not exist "));
        notification.setStatus(true);
        notificationRepository.save(notification);
    }

    @Override
    public List<NotificationDto> getNotificationByRecipient(Long id) {
        UserDto userDto = userService.getUserById(id);
        User user = userMapper.map(userDto, User.class);
        List<Notification> notifications = notificationRepository.getNotificationByRecipient(user);
        return notificationMapper.mapAsList(notifications, NotificationDto.class);

    }

}
