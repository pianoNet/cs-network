package com.pianonet.socialnetwork.controller;

import com.pianonet.socialnetwork.payload.IdRequest;
import com.pianonet.socialnetwork.payload.NotificationDto;
import com.pianonet.socialnetwork.payload.UserDto;
import com.pianonet.socialnetwork.security.CurrentUser;
import com.pianonet.socialnetwork.security.UserPrincipal;
import com.pianonet.socialnetwork.service.FriendListService;
import com.pianonet.socialnetwork.service.NotificationService;
import lombok.AllArgsConstructor;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@AllArgsConstructor
@RequestMapping("/notification")
public class NotificationController {
    private FriendListService friendListService;
    private NotificationService notificationService;

    @GetMapping("/get")
    public List<NotificationDto> getNotification(@CurrentUser UserPrincipal user) {
        return notificationService.getNotificationByRecipient(user.getId());
    }

    @GetMapping("/")
    public List<NotificationDto> getNotificationForMyFriendsPosts(@CurrentUser UserPrincipal user) {
        List<UserDto> friends=friendListService.getFriendListForUser(user.getId());
        return notificationService.getPostNotificationByFriends(friends);
    }
    @PutMapping("/edit")
    public ResponseEntity editStatus(@RequestBody IdRequest friendListId, Errors errors) {
        notificationService.editStatus(friendListId.getId());
        if (errors.hasErrors()) {
            List<String> messages = errors.getAllErrors()
                    .stream()
                    .map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.toList());
            return ResponseEntity.badRequest().body(messages);
        }
        return ResponseEntity.ok().build();
    }


}
