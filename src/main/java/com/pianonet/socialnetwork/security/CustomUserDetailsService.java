package com.pianonet.socialnetwork.security;

import com.pianonet.socialnetwork.exception.ResourceNotFoundException;
import com.pianonet.socialnetwork.model.User;
import com.pianonet.socialnetwork.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {
    private final UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) {
        User user = userRepository.getUserByEmail(username).orElseThrow(
                () -> new ResourceNotFoundException("User does not exist "));
        return UserPrincipal.create(user);
    }

    @Transactional
    public UserDetails loadUserById(Long id) {
        User user = userRepository.findById(id).orElseThrow(
            () -> new RuntimeException(String.format("User not found with id : '%s'", id))
        );

        return UserPrincipal.create(user);
    }
}