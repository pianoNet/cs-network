package com.pianonet.socialnetwork.service.impl;

import com.pianonet.socialnetwork.mappers.UserMapper;
import com.pianonet.socialnetwork.model.Post;
import com.pianonet.socialnetwork.model.User;
import com.pianonet.socialnetwork.payload.PostRequest;
import com.pianonet.socialnetwork.payload.UserDto;
import com.pianonet.socialnetwork.repository.PostRepository;
import com.pianonet.socialnetwork.service.PostService;
import com.pianonet.socialnetwork.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
public class PostServiceTest {
    @MockBean
    private UserService userService;
    @MockBean
    private PostRepository postRepository;
    @Autowired
    private PostService postService;
    @Autowired
    private UserMapper userMapper;
    @Test
    public void testSave(){
        UserDto userDto = UserDto.builder()
                .id(1L)
                .email("sahak17@inbox.ru")
                .firstName("sahak")
                .password("46545646")
                .build();
        User user=userMapper.map(userDto,User.class);
        PostRequest postRequest=PostRequest.builder()
                .content("content")
                .build();
        Post post=Post.builder()
                .user(user)
                .content(postRequest.getContent())
                .build();
        Post post1=Post.builder()
                .id(1L)
                .user(user)
                .content(postRequest.getContent())
                .build();
        when(userService.getUserById(1L)).thenReturn(userDto);
        when(postRepository.save(post)).thenReturn(post1);
        when(postRepository.findById(1L)).thenReturn(java.util.Optional.ofNullable(post));
        assertEquals(post,postService.save(1L,postRequest));
    }
    @Test
    public void testRemoveByID() {
        //TODO create test
    }
    @Test
    public void testUpdatePost() {
        //TODO create test
    }
    @Test
    public void testGetPosts() {
        //TODO create test
    }

}
