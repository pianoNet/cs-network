package com.pianonet.socialnetwork.service.impl;

import com.pianonet.socialnetwork.exception.FileStorageException;
import com.pianonet.socialnetwork.service.FileService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Objects;

@Service
public class FileServiceImpl implements FileService {

    @Value("D:\\")
    public String uploadDir;

    @Override
    public String uploadFile(MultipartFile file, String name) {
        try {

            Path copyLocation = Paths.get(uploadDir + name +"."+ getURLFileFormat(Objects.requireNonNull(file.getOriginalFilename())));
            Files.copy(file.getInputStream(), copyLocation, StandardCopyOption.REPLACE_EXISTING);
            return copyLocation.toString();
        } catch (Exception e) {
            throw new FileStorageException("Could not store file " + file.getOriginalFilename()+ ". Please try again!");
        }
    }

    private String getURLFileFormat(String path) {
        String[] result = path.split("\\.(?=[^.]+$)");
        return result[result.length - 1];
    }
}