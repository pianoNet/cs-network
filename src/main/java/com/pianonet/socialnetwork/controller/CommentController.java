package com.pianonet.socialnetwork.controller;

import com.pianonet.socialnetwork.payload.CommentRequest;
import com.pianonet.socialnetwork.service.CommentService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
public class CommentController {
    private final CommentService commentService;

    @PostMapping("/comment")
    public void createComment(@Valid @RequestBody CommentRequest commentRequest) {
        commentService.addComment(commentRequest);
    }

}