package com.pianonet.socialnetwork.repository;

import com.pianonet.socialnetwork.model.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ImageRepository extends JpaRepository<Image, Long> {
    @Override
    <S extends Image> List<S> saveAll(Iterable<S> iterable);
}
