package com.pianonet.socialnetwork.service.impl;

import com.pianonet.socialnetwork.exception.ResourceNotFoundException;
import com.pianonet.socialnetwork.mappers.UserMapper;
import com.pianonet.socialnetwork.model.User;
import com.pianonet.socialnetwork.payload.UserDto;
import com.pianonet.socialnetwork.payload.auth.AuthResponse;
import com.pianonet.socialnetwork.payload.auth.SignInRequest;
import com.pianonet.socialnetwork.payload.auth.SignUpRequest;
import com.pianonet.socialnetwork.repository.UserRepository;
import com.pianonet.socialnetwork.security.JwtTokenProvider;
import com.pianonet.socialnetwork.service.UserService;
import com.pianonet.socialnetwork.util.MailSender;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final MailSender mailSender;
    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final JwtTokenProvider jwtTokenProvider;
    private final AuthenticationManager authenticationManager;

    @Override
    public void addUser(SignUpRequest upRequest) {
        User user = userMapper.map(upRequest, User.class);
         user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setActivationCode(UUID.randomUUID().toString());
        mailSender.send(user);
        userRepository.save(user);
    }

    @Override
    public AuthResponse signIn(SignInRequest inRequest) {
        User user = userRepository.getUserByEmail(inRequest.getEmail()).orElseThrow(
                () -> new ResourceNotFoundException("user does not exist "));

        if (!passwordEncoder.matches(inRequest.getPassword(), user.getPassword())) {
            return AuthResponse.builder()
                    .message("Wrong email or password")
                    .build();
        }

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        inRequest.getEmail(),
                        inRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String token = jwtTokenProvider.generateToken(authentication);

        return AuthResponse.builder()
                .userId(user.getId())
                .token(token)
                .build();
    }

    @Override
    public UserDto getUserByEmail(String email) {
        User user = userRepository.getUserByEmail(email).orElseThrow(
                () -> new ResourceNotFoundException("User does not exist "));
        return userMapper.map(user, UserDto.class);
    }

    @Override
    public UserDto getUserById(Long id) {
        User user = userRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException(" User does not exist "));
        return userMapper.map(user, UserDto.class);
    }
    @Override
    public void activateUser(String code) {
        User user = userRepository.findByActivationCode(code).orElseThrow(
                () -> new ResourceNotFoundException("Wrong action"));
        user.setActivationCode(null);
        user.setEmailVerified(true);
        userRepository.save(user);
    }
}
