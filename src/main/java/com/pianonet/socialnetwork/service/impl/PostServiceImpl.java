package com.pianonet.socialnetwork.service.impl;

import com.pianonet.socialnetwork.exception.ResourceNotFoundException;
import com.pianonet.socialnetwork.mappers.UserMapper;
import com.pianonet.socialnetwork.model.Post;
import com.pianonet.socialnetwork.model.User;
import com.pianonet.socialnetwork.payload.PostRequest;
import com.pianonet.socialnetwork.payload.UserDto;
import com.pianonet.socialnetwork.repository.PostRepository;
import com.pianonet.socialnetwork.service.ImageService;
import com.pianonet.socialnetwork.service.PostService;
import com.pianonet.socialnetwork.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class PostServiceImpl implements PostService {
    private final PostRepository postRepository;
    private final ImageService imageService;
    private final UserService userService;
    private final UserMapper userMapper;

    @Override
    public Page<Post> getPosts(Pageable pageable) {
        return postRepository.findAll(pageable);
    }

    @Override
    public Post save(Long id, PostRequest postRequest) {
        UserDto userDto = userService.getUserById(id);
        User user = userMapper.map(userDto, User.class);
        Post post = Post.builder()
                .user(user)
                .content(postRequest.getContent())
                .visibility(postRequest.isVisibility())
                .build();
        post = postRepository.save(post);
        if (postRequest.getImages() != null) {
            imageService.save(postRequest.getImages(), post);
        }
        return post;
    }

    @Override
    public ResponseEntity<?> removeByID(Long postId) {
        return postRepository.findById(postId).map(post -> {
            postRepository.delete(post);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("PostId " + postId + " not found"));
    }

    @Override
    public void updatePost(PostRequest postRequest, Long postId) {
      postRepository.findById(postId).map(post -> {
            post.setContent(postRequest.getContent());
            post.setVisibility(postRequest.isVisibility());
            return postRepository.save(post);
        }).orElseThrow(() -> new ResourceNotFoundException("PostId " + postId + " not found"));
    }

}
