package com.pianonet.socialnetwork.repository;

import com.pianonet.socialnetwork.model.Notification;
import com.pianonet.socialnetwork.model.NotificationType;
import com.pianonet.socialnetwork.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface NotificationRepository extends JpaRepository<Notification, Long> {
    @Query("Select u from Notification u where u.recipient= :userId ORDER BY u.creatDate  ")
    List<Notification> getNotificationByRecipient(@Param("userId") User userId);
    @Query("Select u from Notification u where u.notificationType=:notificationType and u.author IN :userId ORDER BY u.creatDate  ")
    List<Notification> getPostNotificationByFriends (@Param("userId") List<User> userId, @Param("notificationType")NotificationType notificationType);
}
