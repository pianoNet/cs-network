package com.pianonet.socialnetwork.payload;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Size;
import java.util.List;
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class PostRequest {

    @Size(min=2, max=50)
    private String content;
    private boolean visibility;
    //TODO image format validation
    private List<MultipartFile> images;

}
