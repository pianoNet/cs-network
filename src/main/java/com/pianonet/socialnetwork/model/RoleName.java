package com.pianonet.socialnetwork.model;

public enum  RoleName {
    ROLE_USER,
    ROLE_ADMIN
}
