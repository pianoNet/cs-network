package com.pianonet.socialnetwork.payload.auth;

import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class SignInRequest {
    @Email
    @NotNull(message = "Email can't be null")
    private String email;

    @Pattern(regexp = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$")
    @NotNull(message = "Password can't be null")
    private String password;

}
