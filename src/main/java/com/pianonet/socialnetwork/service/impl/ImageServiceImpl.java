package com.pianonet.socialnetwork.service.impl;


import com.pianonet.socialnetwork.model.Image;
import com.pianonet.socialnetwork.model.Post;
import com.pianonet.socialnetwork.repository.ImageRepository;
import com.pianonet.socialnetwork.service.FileService;
import com.pianonet.socialnetwork.service.ImageService;
import com.pianonet.socialnetwork.util.UniqueNameGenerator;
import lombok.Data;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@Service
@Data
public class ImageServiceImpl implements ImageService {
    private final FileService fileService;
    private final ImageRepository imageRepository;

    @Override
    public List<Image> save(List<MultipartFile> imgFiles, Post post) {
        List<Image> images = new ArrayList<>();
        for (String url : uploadImages(imgFiles)) {
            Image image = new Image();
            image.setImgPath(url);
            image.setPost(post);
            images.add(image);
        }
        return imageRepository.saveAll(images);
    }

    private List<String> uploadImages(List<MultipartFile> imgFiles) {
        List<String> imageUrl = new ArrayList<>();
        imgFiles.forEach(file -> imageUrl.add(
                fileService.uploadFile(file, UniqueNameGenerator.generateName("img"))
        ));
        return imageUrl;
    }
}