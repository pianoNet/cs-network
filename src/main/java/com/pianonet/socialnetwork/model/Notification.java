package com.pianonet.socialnetwork.model;


import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Date;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class Notification {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(nullable = false)
    private NotificationType notificationType;
    @Column(nullable = false)
    private Long eventId;
    @Column(nullable = false)
    private boolean status;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_author", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private User author;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_recipient", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private User recipient;
    @CreationTimestamp
    private Date creatDate;
}


