package com.pianonet.socialnetwork.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public final class UniqueNameGenerator {
    public static String generateName(String signatureKey) {
        long nowDate = System.currentTimeMillis();
        return signatureKey + nowDate;
    }
}
