package com.pianonet.socialnetwork.service.impl;

import com.pianonet.socialnetwork.exception.ResourceNotFoundException;
import com.pianonet.socialnetwork.mappers.NotificationMapper;
import com.pianonet.socialnetwork.mappers.UserMapper;
import com.pianonet.socialnetwork.model.Notification;
import com.pianonet.socialnetwork.model.NotificationType;
import com.pianonet.socialnetwork.model.User;
import com.pianonet.socialnetwork.payload.NotificationDto;
import com.pianonet.socialnetwork.payload.UserDto;
import com.pianonet.socialnetwork.repository.NotificationRepository;
import com.pianonet.socialnetwork.service.NotificationService;
import com.pianonet.socialnetwork.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;


@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
public class NotificationServiceTest {
    @Autowired
    private NotificationMapper notificationMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private NotificationService notificationService;
    @MockBean
    private UserService userService;
    @MockBean
    private NotificationRepository notificationRepository;
    @Test
    public void testGetPostNotificationByFriends() {
        UserDto user = UserDto.builder()
                .id(2L)
                .email("sahak17@inbox.ru")
                .firstName("sahak")
                .password("encodedPassword")
                .build();
        List<UserDto> friendsDto=new ArrayList<>();
        friendsDto.add(user);
        List<Notification> notifications=new ArrayList<>();
        Notification notification=Notification.builder()
                .eventId(1L)
                .notificationType(NotificationType.POST)
                .id(1L)
                .build();
        notifications.add(notification);
        List<NotificationDto> notificationsDto=notificationMapper.mapAsList(notifications,NotificationDto.class);
        List<User> friends=userMapper.mapAsList(friendsDto,User.class);
        when(notificationRepository.getPostNotificationByFriends(friends, NotificationType.POST)).thenReturn(notifications);
        assertEquals(notificationsDto,notificationService.getPostNotificationByFriends(friendsDto));
    }

    @Test
    public void testGetNotificationByRecipient() {
        UserDto userDto = UserDto.builder()
                .id(2L)
                .email("sahak17@inbox.ru")
                .firstName("sahak")
                .password("encodedPassword")
                .build();
        User user=userMapper.map(userDto,User.class);
        Notification notification=Notification.builder()
                .id(1L)
                .recipient(user)
                .build();
        List<Notification> notifications=new ArrayList<>();
        notifications.add(notification);
        when(userService.getUserById(userDto.getId())).thenReturn(userDto);
        when(notificationRepository.getNotificationByRecipient(user)).thenReturn(notifications);
        List<NotificationDto> notificationsDto=notificationMapper.mapAsList(notifications,NotificationDto.class);
        assertEquals(notificationsDto,notificationService.getNotificationByRecipient(userDto.getId()));
    }
    @Test(expected = ResourceNotFoundException.class)
    public void testEditStatusWhenItsNotExist(){
        when(notificationRepository.findById(3L)).thenReturn(java.util.Optional.ofNullable(null));
        notificationService.editStatus(3L);
    }
}
