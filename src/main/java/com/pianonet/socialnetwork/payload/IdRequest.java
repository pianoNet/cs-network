package com.pianonet.socialnetwork.payload;

import lombok.Data;

@Data
public class IdRequest {
   private Long id;
}
