package com.pianonet.socialnetwork.service;

import com.pianonet.socialnetwork.model.Image;
import com.pianonet.socialnetwork.model.Post;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ImageService {
    List<Image> save(List<MultipartFile> images, Post post);
}
