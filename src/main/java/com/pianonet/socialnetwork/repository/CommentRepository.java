package com.pianonet.socialnetwork.repository;

import com.pianonet.socialnetwork.model.Comment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentRepository extends JpaRepository<Comment, Long> {
}
