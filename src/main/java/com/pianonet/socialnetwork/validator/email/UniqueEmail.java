package com.pianonet.socialnetwork.validator.email;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({FIELD})
@Retention(RUNTIME)
@Constraint(validatedBy = EmailValidator.class)
@Documented
public @interface UniqueEmail {
    String message() default "Email is busy.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
