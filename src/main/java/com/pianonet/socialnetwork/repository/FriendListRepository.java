package com.pianonet.socialnetwork.repository;

import com.pianonet.socialnetwork.model.FriendList;
import com.pianonet.socialnetwork.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface FriendListRepository extends JpaRepository<FriendList, Long> {

    void deleteById(Long id);

    @Query("select p from FriendList p  where p.fromUser=:userId or  p.toUser=:userId")
    List<FriendList> getFriendListWhereUserId(@Param("userId") User userId);

    @Query("select p from FriendList p  where (p.toUser=:userId1 and p.fromUser=:userId2)or (p.toUser=:userId2 and p.fromUser=:userId1) ")
    FriendList alreadyFriends(@Param("userId1") User user1, @Param("userId2") User user2);
}
