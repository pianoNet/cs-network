package com.pianonet.socialnetwork.service.impl;


import com.pianonet.socialnetwork.exception.InvalidActionException;
import com.pianonet.socialnetwork.exception.ResourceNotFoundException;
import com.pianonet.socialnetwork.mappers.UserMapper;
import com.pianonet.socialnetwork.model.FriendList;
import com.pianonet.socialnetwork.model.User;
import com.pianonet.socialnetwork.payload.UserDto;
import com.pianonet.socialnetwork.repository.FriendListRepository;
import com.pianonet.socialnetwork.service.FriendListService;
import com.pianonet.socialnetwork.service.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
public class FriendListServiceTest {
    @Autowired
    private FriendListService friendListService;
    @Autowired
    private UserMapper userMapper;
    @MockBean
    private UserService userService;
    @MockBean
    private FriendListRepository friendListRepository;

    @Test(expected = InvalidActionException.class)
    public void testAddFriendRequestWhenTheyAreAlreadyFriends() {
        User user1 = User.builder()
                .id(1L)
                .email("sahak17@inbox.ru")
                .firstName("sahak")
                .password("46545646")
                .build();
        User user2 = User.builder()
                .id(2L)
                .email("sahakyan17@inbox.ru")
                .firstName("sahak2")
                .password("46545646465467987")
                .build();
        FriendList friendList = FriendList.builder()
                .id(1L)
                .build();
        UserDto userDto1 = userMapper.map(user1, UserDto.class);
        UserDto userDto2 = userMapper.map(user2, UserDto.class);
        when(userService.getUserById(1L)).thenReturn(userDto1);
        when(userService.getUserById(2L)).thenReturn(userDto2);
        //when They are already friends method returns friend list
        when(friendListRepository.alreadyFriends(any(User.class), any(User.class))).thenReturn(friendList);
        friendListService.addFriendRequest(1L, 2L);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testEditStatusWhenItsNotExist() {
        when(friendListRepository.findById(1L)).thenThrow(new ResourceNotFoundException(" Friend List does not exist "));
        friendListService.editStatus(1L);
    }

    @Test
    public void testGetFriendListForUser() {
        User user = User.builder()
                .id(1L)
                .email("sahak17@inbox.ru")
                .firstName("sahak")
                .password("46545646")
                .build();
        User user2 = User.builder()
                .id(2L)
                .email("sahakyan17@inbox.ru")
                .firstName("sahak2")
                .password("46545646465467987")
                .build();
        FriendList friendList = FriendList.builder()
                .id(1L)
                .fromUser(user)
                .toUser(user2)
                .build();
        List<FriendList> friendLists = new ArrayList<>();
        friendLists.add(friendList);
        List<UserDto> friends=new ArrayList<>();
        UserDto me = userMapper.map(user, UserDto.class);
        UserDto friend = userMapper.map(user2, UserDto.class);
        User userMe = userMapper.map(me, User.class);
        friends.add(friend);
        when(userService.getUserById(1L)).thenReturn(me);
        when(friendListRepository.getFriendListWhereUserId(userMe)).thenReturn(friendLists);
        Assert.assertEquals(friends, friendListService.getFriendListForUser(1L));
    }
}
