package com.pianonet.socialnetwork.service.impl;


import com.pianonet.socialnetwork.model.Comment;
import com.pianonet.socialnetwork.payload.CommentRequest;
import com.pianonet.socialnetwork.repository.CommentRepository;
import com.pianonet.socialnetwork.service.CommentService;
import com.pianonet.socialnetwork.service.NotificationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
public class CommentServiceTest {
    @Autowired
    private CommentService commentService;
    @MockBean
    private CommentRepository commentRepository;
    @MockBean
    private NotificationService notificationService;

    @Test
    public void testAddComment() {
        Comment comment = Comment.builder()
                .content("hshfhs")
                .id(1L)
                .build();
        CommentRequest commentRequest = CommentRequest.builder()
                .content("hshfhs")
                .build();

        when(commentRepository.save(any(Comment.class))).thenReturn(comment);
        assertEquals(comment, commentService.addComment(commentRequest));

    }

}
