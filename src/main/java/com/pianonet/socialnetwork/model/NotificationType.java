package com.pianonet.socialnetwork.model;

public enum NotificationType {
    COMMENT,
    FRIEND_REQUEST,
    POST
}
