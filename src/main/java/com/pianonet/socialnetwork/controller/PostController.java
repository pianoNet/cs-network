package com.pianonet.socialnetwork.controller;

import com.pianonet.socialnetwork.model.Post;
import com.pianonet.socialnetwork.payload.PostRequest;
import com.pianonet.socialnetwork.security.CurrentUser;
import com.pianonet.socialnetwork.security.UserPrincipal;
import com.pianonet.socialnetwork.service.PostService;
import lombok.Data;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@Data
public class PostController {
    private final PostService postService;

    @GetMapping("/posts")
    public Page<Post> getAllPosts(Pageable pageable) {
        return postService.getPosts(pageable);
    }

    @PostMapping("/posts")
    public Post createPost(@CurrentUser UserPrincipal userPrincipal, PostRequest postRequest) {
        return postService.save(userPrincipal.getId(),postRequest);
    }

    @PutMapping("/posts/{postId}")
    public void updatePost(@PathVariable Long postId, @Valid @RequestBody PostRequest postRequest) {
        postService.updatePost(postRequest,postId);
    }

    @DeleteMapping("/posts/{postId}")
    public ResponseEntity deletePost(@PathVariable Long postId) {
        return postService.removeByID(postId);
    }
}