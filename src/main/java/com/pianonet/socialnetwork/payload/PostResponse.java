package com.pianonet.socialnetwork.payload;


import com.pianonet.socialnetwork.model.Comment;
import com.pianonet.socialnetwork.model.Image;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Builder
@Setter
@Getter
@ToString
public class PostResponse {
    private Long id;
    private String content;
    private Long userID;
    private boolean visibility;
    private List<Image> images;
    private List<Comment> comments;
}
