package com.pianonet.socialnetwork.controller;

import com.pianonet.socialnetwork.payload.IdRequest;
import com.pianonet.socialnetwork.payload.UserDto;
import com.pianonet.socialnetwork.security.CurrentUser;
import com.pianonet.socialnetwork.security.UserPrincipal;
import com.pianonet.socialnetwork.service.FriendListService;
import lombok.AllArgsConstructor;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@AllArgsConstructor
public class FriendController {

    private FriendListService friendListService;



    @GetMapping("/get")
    public ResponseEntity getFriendListForUser(@CurrentUser UserPrincipal userPrincipal) {
        List<UserDto> friends = friendListService.getFriendListForUser(userPrincipal.getId());
        return ResponseEntity.ok().body(friends);
    }

    @PostMapping("/addFriendRequest")
    public ResponseEntity addFriendRequest(@CurrentUser UserPrincipal fromUser, @RequestBody IdRequest toUserId, Errors errors) {
        friendListService.addFriendRequest(fromUser.getId(), toUserId.getId());
        if (errors.hasErrors()) {
            List<String> messages = errors.getAllErrors()
                    .stream()
                    .map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.toList());
            return ResponseEntity.badRequest().body(messages);
        }

        return ResponseEntity.ok().build();
    }

    @PutMapping("/edit")
    public ResponseEntity editStatus(@RequestBody IdRequest friendListId, Errors errors) {
        friendListService.editStatus(friendListId.getId());
        if (errors.hasErrors()) {
            List<String> messages = errors.getAllErrors()
                    .stream()
                    .map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.toList());
            return ResponseEntity.badRequest().body(messages);
        }
        return ResponseEntity.ok().build();
    }
}