package com.pianonet.socialnetwork.service.impl;


import com.pianonet.socialnetwork.exception.InvalidActionException;
import com.pianonet.socialnetwork.exception.ResourceNotFoundException;
import com.pianonet.socialnetwork.mappers.UserMapper;
import com.pianonet.socialnetwork.model.FriendList;
import com.pianonet.socialnetwork.model.Notification;
import com.pianonet.socialnetwork.model.NotificationType;
import com.pianonet.socialnetwork.model.User;
import com.pianonet.socialnetwork.payload.UserDto;
import com.pianonet.socialnetwork.repository.FriendListRepository;
import com.pianonet.socialnetwork.repository.UserRepository;
import com.pianonet.socialnetwork.service.FriendListService;
import com.pianonet.socialnetwork.service.NotificationService;
import com.pianonet.socialnetwork.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class FriendListServiceImpl implements FriendListService {
private final UserRepository userRepository;
    private final FriendListRepository friendListRepository;
    private final NotificationService notificationService;
    private final UserService userService;
    private final UserMapper userMapper;

    @Override
    public void addFriendRequest(Long fromUser, Long toUser) {
        FriendList friendList = createFriendList(fromUser, toUser);

        if (friendListRepository.alreadyFriends(friendList.getToUser(), friendList.getFromUser()) != null) {
            throw new InvalidActionException("They are already friends");
        }
        FriendList savedFriendList = friendListRepository.save(friendList);
        Notification notification = Notification.builder()
                .notificationType(NotificationType.FRIEND_REQUEST)
                .author(savedFriendList.getFromUser())
                .recipient(savedFriendList.getToUser())
                .eventId(savedFriendList.getId())
                .build();

        notificationService.addNotification(notification);
    }

    @Override
    public void editStatus(Long friendListId) {
        FriendList friendList = friendListRepository.findById(friendListId).orElseThrow(
                () -> new ResourceNotFoundException(" Friend List does not exist "));
        friendList.setStatus(true);
        friendListRepository.save(friendList);
    }

    @Override
    public List<UserDto> getFriendListForUser(Long userId) {
        UserDto userDto = userService.getUserById(userId);
        User user = userMapper.map(userDto, User.class);
        List<FriendList> friendsList = friendListRepository.getFriendListWhereUserId(user);
        List<User> friends = friendsList.stream().map(FriendList::getFromUser).collect(Collectors.toList());
        friends.addAll(friendsList.stream().map(FriendList::getToUser).collect(Collectors.toList()));
        List<UserDto> friendsDto = userMapper.mapAsList(friends, UserDto.class);
        return friendsDto.stream()
                .filter(c -> !c.equals(userDto))
                .collect(Collectors.toList());
    }

    private FriendList createFriendList(Long fromUserId, Long toUserId) {
        if (fromUserId.equals(toUserId)) {
            throw new InvalidActionException("you can not send friend request to yourself");
        }
        User fromUser = userMapper.map(userService.getUserById(fromUserId), User.class);
        User toUser = userMapper.map(userService.getUserById(toUserId), User.class);

        return FriendList.builder()
                .fromUser(fromUser)
                .toUser(toUser)
                .build();
    }
}
