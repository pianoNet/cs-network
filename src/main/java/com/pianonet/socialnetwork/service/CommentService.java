package com.pianonet.socialnetwork.service;

import com.pianonet.socialnetwork.model.Comment;
import com.pianonet.socialnetwork.payload.CommentRequest;

public interface CommentService {
    Comment addComment(CommentRequest commentRequest);
}
