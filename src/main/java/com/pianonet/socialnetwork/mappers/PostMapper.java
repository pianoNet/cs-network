package com.pianonet.socialnetwork.mappers;

import com.pianonet.socialnetwork.model.Post;
import com.pianonet.socialnetwork.payload.PostRequest;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;

@Component
public class PostMapper extends ConfigurableMapper {

    @Override
    protected void configure(MapperFactory factory) {
        factory.classMap(PostRequest.class, Post.class).byDefault().register();
    }
}
