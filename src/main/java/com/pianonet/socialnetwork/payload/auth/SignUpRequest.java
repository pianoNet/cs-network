package com.pianonet.socialnetwork.payload.auth;

import com.pianonet.socialnetwork.validator.email.UniqueEmail;
import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class SignUpRequest {
    @Email(message = "Email is not valid")
    @UniqueEmail(message = "Email is already exist")
    @NotNull(message = "Email can't be null")
    private String email;

    @Pattern(regexp = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$", message = "Password must have 8-10 characters")
    @NotNull(message = "Password can't be null")
    private String password;

    @Size(min = 3, max = 50, message = "Name characters size must be between 3 and 50")
    @NotNull(message = "Name can't be null")
    private String firstName;

}
