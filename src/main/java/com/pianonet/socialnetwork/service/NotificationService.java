package com.pianonet.socialnetwork.service;


import com.pianonet.socialnetwork.model.Notification;
import com.pianonet.socialnetwork.payload.NotificationDto;
import com.pianonet.socialnetwork.payload.UserDto;

import java.util.List;

public interface NotificationService {

    void addNotification(Notification notification);

    List<NotificationDto> getNotificationByRecipient(Long id);

    List<NotificationDto> getPostNotificationByFriends(List<UserDto> friendsDto);

    void editStatus(Long id);
}
