package com.pianonet.socialnetwork.service.impl;

import com.pianonet.socialnetwork.mappers.CommentMapper;
import com.pianonet.socialnetwork.model.Comment;
import com.pianonet.socialnetwork.model.Notification;
import com.pianonet.socialnetwork.model.NotificationType;
import com.pianonet.socialnetwork.payload.CommentRequest;
import com.pianonet.socialnetwork.repository.CommentRepository;
import com.pianonet.socialnetwork.service.CommentService;
import com.pianonet.socialnetwork.service.NotificationService;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Service;

@Service
@Data
public class CommentServiceImpl implements CommentService {
    private final CommentRepository commentRepository;
    private final NotificationService notificationService;
    private final CommentMapper commentMapper;

    @Override
    public Comment addComment(CommentRequest commentRequest) {
        Comment comment = commentMapper.map(commentRequest, Comment.class);
        Comment savedComment = commentRepository.save(comment);
        Notification notification = Notification.builder()
                .notificationType(NotificationType.COMMENT)
                .author(savedComment.getUser())
                .eventId(savedComment.getId())
                .build();
        notificationService.addNotification(notification);
        return savedComment;
    }
}