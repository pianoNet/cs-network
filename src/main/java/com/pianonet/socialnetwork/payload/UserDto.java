package com.pianonet.socialnetwork.payload;

import com.pianonet.socialnetwork.validator.email.UniqueEmail;
import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Date;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@EqualsAndHashCode
public class UserDto {
    private Long id;

    @Email(message = "Email is not valid")
    @UniqueEmail(message = "Email is already exists")
    private String email;

    @Pattern(regexp = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$", message = "Password must have 8-10 characters")
    private String password;

    @Size(min = 3, max = 50, message = "Name characters size must be between 3 and 50")
    private String firstName;

    private String lastName;

    private String activationCode;

    private String avatarImgPath;

    private String phoneNumber;

    private Date createdDate;
}
