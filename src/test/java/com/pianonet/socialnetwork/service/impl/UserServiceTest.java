package com.pianonet.socialnetwork.service.impl;


import com.pianonet.socialnetwork.exception.ResourceNotFoundException;
import com.pianonet.socialnetwork.mappers.UserMapper;
import com.pianonet.socialnetwork.model.User;
import com.pianonet.socialnetwork.payload.UserDto;
import com.pianonet.socialnetwork.payload.auth.AuthResponse;
import com.pianonet.socialnetwork.payload.auth.SignInRequest;
import com.pianonet.socialnetwork.payload.auth.SignUpRequest;
import com.pianonet.socialnetwork.repository.UserRepository;
import com.pianonet.socialnetwork.security.JwtTokenProvider;
import com.pianonet.socialnetwork.service.UserService;
import com.pianonet.socialnetwork.util.MailSender;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
public class UserServiceTest {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserService userService;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @MockBean
    private UserRepository userRepository;
    @MockBean
    private JwtTokenProvider jwtTokenProvider;
    @MockBean
    private MailSender mailSender;
    @MockBean
    private AuthenticationManager authenticationManager;

    @Test
    public void TestAddUser() {
        //TODO create test
    }

    @Test(expected = ResourceNotFoundException.class)
    public void TestEmailActivationWhenWrongActivationCode() {
        when(userRepository.findByActivationCode("Wrong code")).thenReturn(java.util.Optional.ofNullable(null));
        userService.activateUser("Wrong code");
    }

    @Test
    public void testCorrectLogin() {
        String token = "token";
        SignInRequest sign = SignInRequest.builder()
                .email("sahak17@inbox.ru")
                .password("sahak17")
                .build();
        String encodedPassword = passwordEncoder.encode(sign.getPassword());
        User user = User.builder()
                .id(2L)
                .email("sahak17@inbox.ru")
                .firstName("sahak")
                .password(encodedPassword)
                .build();
        AuthResponse authResponse = AuthResponse.builder()
                .userId(user.getId())
                .token(token)
                .build();
        when(userRepository.getUserByEmail(any(String.class))).thenReturn(java.util.Optional.of(user));
        when(jwtTokenProvider.generateToken(any())).thenReturn(token);
        assertEquals(authResponse, userService.signIn(sign));

    }

    @Test
    public void testLoginWithWrongPassword() {
        SignInRequest sign = SignInRequest.builder()
                .email("sahak17@inbox.ru")
                .password("Wrong")
                .build();
        String encodedPassword = passwordEncoder.encode("sahak17");
        User user = User.builder()
                .id(2L)
                .email("sahak17@inbox.ru")
                .firstName("sahak")
                .password(encodedPassword)
                .build();
        AuthResponse authResponse = AuthResponse.builder()
                .message("Wrong email or password")
                .build();
        when(userRepository.getUserByEmail(any(String.class))).thenReturn(java.util.Optional.ofNullable(user));
        assertEquals(authResponse, userService.signIn(sign));
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testLoginWithWrongEmail() {
        SignInRequest sign = SignInRequest.builder()
                .email("Wrong")
                .password("sahak17")
                .build();
        when(userRepository.getUserByEmail(any(String.class))).thenReturn(java.util.Optional.ofNullable(null));
        userService.signIn(sign);
    }

    @Test
    public void testGetUserByEmail() {
        String encodedPassword = passwordEncoder.encode("sahak17");
        User user = User.builder()
                .id(1L)
                .email("sahak17@inbox.ru")
                .firstName("sahak")
                .password(encodedPassword)
                .build();
        String email = "sahak17@inbox.ru";
        UserDto userDto = userMapper.map(user, UserDto.class);
        when(userRepository.getUserByEmail(email)).thenReturn(java.util.Optional.ofNullable(user));
        assertEquals(userDto, userService.getUserByEmail(email));
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testGetUserByEmailWhenItsNotExist() {
        String email = "sahak17@inbox.ru";
        when(userRepository.getUserByEmail(email)).thenReturn(java.util.Optional.ofNullable(null));
        userService.getUserByEmail(email);
    }

    @Test
    public void testGetUserById() {
        String encodedPassword = passwordEncoder.encode("sahak17");
        User user = User.builder()
                .id(1L)
                .email("sahak17@inbox.ru")
                .firstName("sahak")
                .password(encodedPassword)
                .build();

        UserDto userDto = userMapper.map(user, UserDto.class);
        when(userRepository.findById(user.getId())).thenReturn(java.util.Optional.ofNullable(user));
        assertEquals(userDto, userService.getUserById(user.getId()));
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testGetUserByIdWhenItsNotExist() {
        Long id = 1L;
        when(userRepository.findById(id)).thenReturn(java.util.Optional.ofNullable(null));
        userService.getUserById(id);
    }
}
