package com.pianonet.socialnetwork.service;


import com.pianonet.socialnetwork.payload.UserDto;

import java.util.List;

public interface FriendListService {
    void addFriendRequest(Long fromUser, Long toUser);

    List<UserDto> getFriendListForUser(Long userId);

    void editStatus(Long friendListId);
}
