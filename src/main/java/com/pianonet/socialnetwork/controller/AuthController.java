package com.pianonet.socialnetwork.controller;

import com.pianonet.socialnetwork.payload.auth.AuthResponse;
import com.pianonet.socialnetwork.payload.auth.SignInRequest;
import com.pianonet.socialnetwork.payload.auth.SignUpRequest;
import com.pianonet.socialnetwork.service.UserService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {
    private final UserService userService;
    private static final Logger logger = LoggerFactory.getLogger(AuthController.class);
    private AuthResponse authResponse;
    @GetMapping("/activate/{code}")
    public ResponseEntity activate(@PathVariable String code) {

        userService.activateUser(code);
        authResponse = AuthResponse.builder()
                .message("Your email was successfully confirmed")
                .build();
        return ResponseEntity.ok().body(authResponse);

    }
    @PostMapping("/signup")
    public ResponseEntity registerUser(@Valid @RequestBody SignUpRequest signUpRequest, Errors errors) {

        if (errors.hasErrors()) {
            List<String> messages = errors.getAllErrors()
                    .stream()
                    .map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.toList());

            authResponse = AuthResponse.builder()
                    .messages(messages)
                    .build();
            return ResponseEntity.badRequest().body(authResponse);
        }

        userService.addUser(signUpRequest);

        authResponse = AuthResponse.builder()
                .message("User successfully registered.")
                .build();

        URI location = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/signin")
                .build()
                .toUri();

        return ResponseEntity.created(location).body(authResponse);
    }

    @PostMapping("/signin")
    public ResponseEntity authenticateUser(@Valid @RequestBody SignInRequest signInRequest, Errors errors) {
        URI location;
        if (errors.hasErrors()) {
            List<String> messages = errors.getAllErrors()
                    .stream()
                    .map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.toList());

            authResponse = AuthResponse.builder()
                    .messages(messages)
                    .build();

            return ResponseEntity.badRequest().body(authResponse);
        }

        authResponse = userService.signIn(signInRequest);

        location = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/user/me")
                .buildAndExpand(authResponse.getUserId())
                .toUri();

        return ResponseEntity.ok().location(location).body(authResponse);
    }

}
