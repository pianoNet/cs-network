package com.pianonet.socialnetwork.payload;

import com.pianonet.socialnetwork.model.Comment;
import com.pianonet.socialnetwork.model.Post;
import com.pianonet.socialnetwork.model.User;
import lombok.*;

import java.util.Date;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class CommentRequest {

    private Long id;

    private Post post;

    private User user;

    private String content;

    private Comment parentComment;

    private Date createdDate;

}