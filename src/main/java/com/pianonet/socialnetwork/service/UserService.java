package com.pianonet.socialnetwork.service;

import com.pianonet.socialnetwork.payload.UserDto;
import com.pianonet.socialnetwork.payload.auth.AuthResponse;
import com.pianonet.socialnetwork.payload.auth.SignInRequest;
import com.pianonet.socialnetwork.payload.auth.SignUpRequest;

public interface UserService {
    void activateUser(String code);

    void addUser(SignUpRequest upRequest);

    AuthResponse signIn(SignInRequest inRequest);

    UserDto getUserByEmail(String email);

    UserDto getUserById(Long id);
}
