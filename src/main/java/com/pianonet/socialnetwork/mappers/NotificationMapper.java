package com.pianonet.socialnetwork.mappers;

import com.pianonet.socialnetwork.model.Notification;
import com.pianonet.socialnetwork.payload.NotificationDto;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.stereotype.Component;

@Component
public class NotificationMapper extends ConfigurableMapper {
    @Override
    protected void configure(MapperFactory factory) {

        factory.classMap(Notification.class, NotificationDto.class)
                .byDefault()
                .register();
    }
}
