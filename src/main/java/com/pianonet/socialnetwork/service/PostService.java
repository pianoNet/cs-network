package com.pianonet.socialnetwork.service;

import com.pianonet.socialnetwork.model.Post;
import com.pianonet.socialnetwork.payload.PostRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

public interface PostService {

    Page<Post> getPosts(Pageable pageable);

    Post save(Long id, PostRequest postRequest);

    ResponseEntity removeByID(Long postId);

    void updatePost(PostRequest postRequest, Long postId);
}
